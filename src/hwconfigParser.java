import java.io.*;
import org.w3c.dom.*;

import javax.xml.XMLConstants;
import javax.xml.parsers.*;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;


public class hwconfigParser {

	public static void main(String[] args) {
		
	    try {

	        File fXmlFile = new File("hwconfig.xml");
	        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	        Document doc = dBuilder.parse(fXmlFile);
	                
	        //optional, but recommended
	        //read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
	        doc.getDocumentElement().normalize();

	        System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
	                
	        NodeList nList = doc.getElementsByTagName("omron-configuration");
	                
	        System.out.println("----------------------------");
	        
	        for (int temp = 0; temp < nList.getLength(); temp++) {
	        	System.out.println(temp);
	            Node nNode = nList.item(temp);
	                    
	            System.out.println("\nCurrent Element :" + nNode.getNodeName());
	                    
	            if (nNode.getNodeType() == Node.ELEMENT_NODE) {

	                Element eElement = (Element) nNode;
	                System.out.println(eElement);
//	                System.out.println("Enabled : " + eElement.getAttribute("enabled"));
//	                System.out.println("First Name : " + eElement.getElementsByTagName("firstname").item(0).getTextContent());
//	                System.out.println("Last Name : " + eElement.getElementsByTagName("lastname").item(0).getTextContent());
//	                System.out.println("Nick Name : " + eElement.getElementsByTagName("nickname").item(0).getTextContent());
//	                System.out.println("Salary : " + eElement.getElementsByTagName("salary").item(0).getTextContent());

	            }
	        }
	        } catch (Exception e) {
	        e.printStackTrace();
	        }
	      }

}

