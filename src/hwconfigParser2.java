import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class hwconfigParser2 {
  public static void main(String[] args) {

    try {

    File file = new File("hwconfig.xml");

    DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance()
                             .newDocumentBuilder();

    Document doc = dBuilder.parse(file);

    System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
    System.out.println("--------------------------------------");
    
    if (doc.hasChildNodes()) {
    	
        printNote(doc.getChildNodes());

    }

    } catch (Exception e) {
    System.out.println(e.getMessage());
    }

  }

  private static void printNote(NodeList nodeList) {

    for (int count = 0; count < nodeList.getLength(); count++) {
    	
    Node tempNode = nodeList.item(count);

   
    if (tempNode.hasChildNodes()) {

        // loop again if has child nodes
    	System.out.println(tempNode.getNodeName());
    	//System.out.println("Element Node has children : "+ ((Node) tempNode.getChildNodes()).getNodeName());
        printNote(tempNode.getChildNodes());
        
    }

    
    // make sure it's element node.
    if (tempNode.getNodeType() == Node.ELEMENT_NODE) {

        // get node name and value
        System.out.println("\nNode Name =" + tempNode.getNodeName() + " [OPEN]");
        
        if (tempNode.hasAttributes()) {
        	
        	
        	System.out.println("\nNode Name = " + tempNode.getNodeName());
            // get attributes names and values
            NamedNodeMap nodeMap = tempNode.getAttributes();

            for (int i = 0; i < nodeMap.getLength(); i++) {

                Node node = nodeMap.item(i);
                System.out.println("attr name : " + node.getNodeName());
                System.out.println("attr value : " + node.getNodeValue());

            }

        }
        System.out.println("Node Value =" + tempNode.getTextContent());
        

        
        System.out.println("Node Name =" + tempNode.getNodeName() + " [CLOSE]\n\n");

    }

    }

  }

}